import string 

alphabets = string.ascii_uppercase 

def find_next_word(start, word_list, ladder):
    for i in range(len(start)):
        for c in alphabets:
            next_word = start[:i] + c + start[i+1:]
            if is_legitamate(next_word, word_list, ladder):
                return next_word
    return None

def find_ladder(start, end, word_list):
    start = start
    end = end
    word_list = word_list
    ladder = [start]
    
    while True:
        next_word = find_next_word(start, word_list, ladder)
        if next_word:
            ladder.append(next_word)
            start = next_word
        if start == end:
            return [word for word in ladder]
        if not next_word:
            return None

def load_word_list(filename):
    with open(filename, 'r') as f:
        return [line.strip() for line in f.readlines()]

def main():
    filename = r"sowpods.txt"  
    word_list = load_word_list(filename)
    start_word = 'CLAY'
    end_word = 'GOLD'
    ladder = word_ladder(start_word, end_word, word_list)
    if ladder:
        print(' -> '.join(ladder))
    else:
        print("No ladder found!")

if __name__ == '__main__':
    main()
