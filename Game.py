def next_word(start: str, end: str, word_list):
    word_list = set(word.lower() for word in word_list)
    possible_words = []

    for word in word_list:
        if len(word) == len(start):
            num_of_matching_char = sum(1 for a, b in zip(word, start) if a == b)
            if num_of_matching_char == len(start) - 1:
                possible_words.append(word)

    best_word = ""
    match = 0
    for word in possible_words:
        current_match = sum(1 for a, b in zip(word, end) if a == b)
        if current_match > match:
            match = current_match
            best_word = word

    return best_word

def load_word_list(filename):
    with open(filename, 'r') as f:
        return [line.strip() for line in f.readlines()]

def main():
    filename = r"sowpods.txt"
    word_list = load_word_list(filename)
    start_word = 'AAL'
    end_word = 'AAH'
    ct = 0
    next_word_var = start_word

    while next_word_var != end_word:
        next_word_var = next_word(start_word, end_word, word_list)
        start_word = next_word_var  
        ct += 1

    print(ct)

if __name__ == '__main__':
    main()

